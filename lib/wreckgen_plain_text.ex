defmodule WreckgenPlainText do
  @moduledoc """
  Documentation for `WreckgenPlainText`.
  """

  @random_gen 100

  @pretext """
  # list generated at: #{DateTime.to_string(DateTime.utc_now())} UTC
  # https://wreckgen.scottme.me
  # https://gitlab.com/ScottTheMeme/wreckgen

  """

  @posttext """
  \n</html>
  """

  @priv_opts [
    "banger_races_1",
    "banger_races_2",
    "the_very_track_pack_1_and_2"
  ]

  def gen_all(:all) do
    index = "html/index.html"
    {:ok, file} = File.open(index, [:append])

    for list_name <- @priv_opts do
      gen(list_name, :nocd)
      IO.binwrite(file, "<br><a href=\"/#{list_name}.txt\">#{list_name} plain text</a>")
    end

    IO.binwrite(file, @posttext)
    File.close(file)
    :ok
  end

  def gen(list_name, cd? \\ :nocd) do
    if cd? != :nocd, do: cd()
    path = "html/#{list_name}.txt"
    maps = import_json(list_name)

    if File.exists?(path), do: File.rm!(path)
    {:ok, file} = File.open(path, [:append])

    list = Enum.take_random(maps, @random_gen)
    export_to_file(file, list)
  end

  def export_to_file(file, list) do
    IO.binwrite(file, @pretext)

    for x <- list do
      txt =
        "\nel_add=#{x.name}" <>
          if Map.has_key?(x, :gamemodes) do
            "\nel_gamemode=#{Enum.random(x.gamemodes)}"
          else
            "\n>el_gamemode=racing"
          end <>
          format_key(x, :laps, "el_laps") <>
          format_key(x, :bots, "el_bots") <>
          format_key(x, :car_reset_disabled, "el_car_reset_disabled") <>
          format_key(x, :wrong_way_limiter_disabled, "el_wrong_way_limiter_disabled") <>
          format_key(x, :car_class_restriction, "el_car_class_restriction") <>
          format_key(x, :car_restriction, "el_car_restriction") <>
          format_key(x, :weather, "el_weather")

      IO.binwrite(file, txt)
    end

    File.close(file)
  end

  defp cd do
    :wreckgen
    |> :code.priv_dir()
    |> File.cd!()
  end

  defp format_key(x, key, html_prefix) do
    if Map.has_key?(x, key), do: "\n#{html_prefix}=#{Enum.random(Map.get(x, key))}", else: ""
  end

  def import_json(list_name) do
    File.read!("json/" <> list_name <> ".json")
    |> Jason.decode!()
    |> AtomicMap.convert()
  end
end
