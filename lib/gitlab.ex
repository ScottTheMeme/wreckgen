defmodule GitLab do
  def pages do
    Wreckgen.gen_all(:all, false)
    WreckgenPlainText.gen_all(:all)
  end
end
