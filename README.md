# Wreckgen

This list is regenerated daily to ensure theres always a new loop.

[latest list](https://wreckgen.scottme.me)

## Required Dependencies

elixir 13

Erlang 25


## Donation

XMR 45Mnhwb6ia8CKGWpUAX4cP4HDn9aSoMCm2EaebZf6G6VTz4WRpkt1AELQwwMBPhwEtLVgv48Lu71g7DVSnbSAj6iTTA3K7v

BTC bc1qed9e9hpau89qnz8pcccpuskgspchygl3z3mzch

ETH 0x94DEa9b46Bc0e3B4E16c22781062139B3FEDCE2a

XLM GDPH2B6TFWNGDH5UHFIJITAHPUPJTQWWUVGSSPNWFZKBSA7XIW5LRNO2

DOT 13z5rRvqDAENxhqsyCqHbQktVvjzjLcHcA5R6e7U2Cqoqbgq
